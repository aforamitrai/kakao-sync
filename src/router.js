import { createWebHistory, createRouter } from "vue-router";
import HomeView from "../src/pages/HomeView.vue";
import MapView from "../src/pages/MapView.vue";
const routes = [
  {
    path: "/",
    name: "home-view",
    component: HomeView,
  },
  {
    path: "/map-view",
    name: "map-view",
    component: MapView,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});
export default router;
