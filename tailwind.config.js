module.exports = {
  purge: ["./src/components/*.vue"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      width: {
        "app-w": "375px",
        "app-primary": "50px",
      },
      height: {
        "app-h": "812px",
        "app-primary": "50px",
        card: "130px",
      },
      colors: {
        "app-blue": "#06205c",
      },
      fontSize: {
        x: "8px",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
